extends Path2D
class_name PathVisualizer2D

export(float, 0.0, 16.0) var inward_dist := 10.0 setget set_inward_dist
export(float, 0.0, 32.0) var in_out_dist := 12.0 setget set_in_out_dist

onready var path_follower := PathFollow2D.new()
onready var sprite := Sprite.new()
onready var line := Line2D.new()
onready var visualizer_material: ShaderMaterial = load("res://scenes/effects/path_visualizer.material")

func _ready() -> void:
	add_child(path_follower)
	add_child(line)
	path_follower.add_child(sprite)
	sprite.scale = Vector2(0.5, 0.5)
	sprite.texture = load("res://icon.png")
	line.material = visualizer_material
	line.texture_mode = Line2D.LINE_TEXTURE_STRETCH

func _process(delta: float) -> void:
	path_follower.offset += delta * 300.0

func push_position(new_position: Vector2) -> void:
	curve.add_point(new_position)
	
func clear() -> void:
	curve.clear_points()

func set_inward_dist(new_inward_dist: float) -> void:
	inward_dist = new_inward_dist
	rebuild_path()
	
func set_in_out_dist(new_in_out_dist: float) -> void:
	in_out_dist = new_in_out_dist
	rebuild_path()

func rebuild_path() -> void:
	var positions := []
	path_follower.offset = 0.0
	
	for i in range(curve.get_point_count()):
		curve.set_point_position(i, (curve.get_point_position(i) + Vector2(16, 16)).snapped(Vector2(32, 32)) - Vector2(16, 16))
		positions.push_back(curve.get_point_position(i))
		curve.set_point_in(i, Vector2())
		curve.set_point_out(i, Vector2())

	for i in range(1, curve.get_point_count() - 1):
		var point_position: Vector2 = positions[i]
		var next_position: Vector2 = positions[i + 1]
		var prev_position: Vector2 = positions[i - 1]
		var forward_direction := point_position.direction_to(next_position)
		var back_direction := point_position.direction_to(prev_position)
		var inwards_angle_direction: Vector2= Vector2.ZERO if (forward_direction + back_direction).is_equal_approx(Vector2.ZERO) else (forward_direction + back_direction).normalized()
		
		var dir_sign = sign((next_position - point_position).cross(inwards_angle_direction))
		var tangent = inwards_angle_direction.tangent() * dir_sign
		
		curve.set_point_position(i, point_position + inwards_angle_direction * inward_dist)
		curve.set_point_out(i, tangent * in_out_dist)
		curve.set_point_in(i, -tangent * in_out_dist)
	
	line.clear_points()
	
	for point in curve.tessellate():
		line.add_point(point)
