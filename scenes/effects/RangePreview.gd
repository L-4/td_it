extends Node2D
class_name RangePreview2D

func _ready() -> void:
	update()

var is_ok setget set_is_ok

func set_is_ok(new_is_ok: bool) -> void:
	if new_is_ok == is_ok: return
	is_ok = new_is_ok
	if is_ok:
		modulate = Color(0.1, 1.0, 0.0, 0.3)
	else:
		modulate = Color(1.0, 0.0, 0.0, 0.3)

	update()

var radius := 100.0 setget set_radius

func set_radius(new_radius: float) -> void:
	if radius == new_radius: return
	radius = new_radius
	update()

func _draw() -> void:
	draw_circle(Vector2(), radius, Color.white)
