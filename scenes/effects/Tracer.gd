extends MeshInstance2D
class_name Tracer2D

var lifetime_left := 1.0
var lifetime_ratio := 1.0

func _init(from: Vector2, to: Vector2, thickness: float = 1.0, color: Color = Color.yellow, lifetime = 0.2) -> void:
	lifetime_ratio = 1 / lifetime
	
	var ht := thickness / 2.0 # half thickness
	var normal = (to - from).normalized()
	normal = Vector2(normal.y * ht, -normal.x * ht)
	
	var verts = PoolVector3Array([
		Vector3(
			from.x - normal.x,
			from.y - normal.y,
			0),
		Vector3(
			from.x + normal.x,
			from.y + normal.y,
			0),
		Vector3(
			to.x + normal.x,
			to.y + normal.y,
			0),
		Vector3(
			to.x - normal.x,
			to.y - normal.y,
			0),	
	])
	
	var colors := PoolColorArray([color, color, color, color])
	
	var arrays := []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = verts
	arrays[ArrayMesh.ARRAY_COLOR] = colors
	
	var am = ArrayMesh.new()
	am.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLE_FAN, arrays)
	
	mesh = am

func _process(delta: float) -> void:
	lifetime_left -= delta * lifetime_ratio
	
	if lifetime_left <= 0.0:
		queue_free()
	else:
		pass
		modulate = Color(1.0, 1.0, 1.0, lifetime_left)
