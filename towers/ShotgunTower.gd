extends Tower2D

func _ready() -> void:
	tower_name = "shotgun"

const FIRE_RATE := 1.0

var cooldown := 0.0

const SPREAD_RANGE := 0.2 # rad

func get_spread() -> float:
	var spread = randf() * SPREAD_RANGE
	return spread - SPREAD_RANGE / 2.0

# Since this interacts with physics, state, only call from _physics_process
func try_fire() -> void:
	if not has_target(): return
	var target := get_target()

	var delta: float = get_angle_to(target.position) - $Head.rotation
	if abs(delta) > 0.001: return
	var space_state := get_world_2d().direct_space_state
	
	var from := global_position

	for shot in range(8):
		var rot = $Head.rotation + get_spread()
		var dir := Vector2(cos(rot), sin(rot))
		var to = from + dir * 1000.0
		var result = space_state.intersect_ray(from, to, [], GlobalConfig.CollisionLayer.ENEMY)
		
		if result.has("collider") and result.collider is Enemy2D:
			result.collider.deal_damage(1.0)
			to = result.position
		
		var tracer = Tracer2D.new(from, to)
		get_node("/root/Main/Effects").add_child(tracer)

	cooldown = FIRE_RATE

func _physics_process(delta: float) -> void:
	if has_target():
		var target: Enemy2D = get_target()
		var delta_ang := clamp(get_angle_to(target.position) - $Head.rotation, -delta, delta)
		$Head.rotate(delta_ang)

	if cooldown <= 0.0:
		try_fire()
	else:
		cooldown -= delta

