extends Tower2D

func _ready() -> void:
	tower_name = "basic"

const FIRE_RATE := 0.25

var cooldown := 0.0

func try_fire() -> void:
	if not has_target(): return
	var target := get_target()

	var delta: float = get_angle_to(target.position) - $Cannon.rotation
	if abs(delta) > 0.001: return

	target.deal_damage(1.0)
	cooldown = FIRE_RATE
	var tracer = Tracer2D.new(position, target.position)
	get_node("/root/Main/Effects").add_child(tracer)

func _process(delta: float) -> void:
	if has_target():
		var target: Enemy2D = get_target()
		var delta_ang := clamp(get_angle_to(target.position) - $Cannon.rotation, -delta, delta)
		$Cannon.rotate(delta_ang)

	if cooldown <= 0.0:
		try_fire()
	else:
		cooldown -= delta

