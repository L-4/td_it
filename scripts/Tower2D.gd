extends Node2D
class_name Tower2D, "res://scripts/icon_tower_2d.svg"

var tower_name: String

var in_range := []

onready var range_area := get_node("RangeArea") as Area2D

func _ready() -> void:
	range_area.connect("body_entered", self, "on_body_entered")
	range_area.connect("body_exited", self, "on_body_exited")

func on_body_entered(body: Node) -> void:
	assert(body is Enemy2D)
	if body is Enemy2D:
		in_range.push_back(body)

func on_body_exited(body: Node) -> void:
	assert(body is Enemy2D)
	if body is Enemy2D:
		in_range.erase(body)

func get_target() -> Enemy2D:
	return in_range.front() if in_range.size() > 0 else null

func has_target() -> bool: return not in_range.empty()
