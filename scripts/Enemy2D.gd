extends KinematicBody2D
class_name Enemy2D, "res://scripts/icon_enemy_2d.svg"

const ARRIVED_MARGIN := pow(3.0, 2.0)

signal arrived_at_destination(enemy)
signal died(enemy)

var health := 10.0

func deal_damage(damage: float) -> void:
	health -= damage
	
	if health <= 0.0:
		die()
		
func die() -> void:
	emit_signal("died", self)
	queue_free()
	MoneyManager.change_money(10)

var path := Array()

func _ready() -> void:
	PhaseManager.connect("wave_phase_started", self, "on_wave_phase_started")
	PhaseManager.connect("build_phase_started", self, "on_build_phase_started")
	
	path = PathManager.get_path_copy()

func _physics_process(delta: float) -> void:
	if path.empty(): return
	
	var at_destination := true
	while at_destination:
		var destination: Vector2 = path.back()
		
		if position.distance_squared_to(destination) < ARRIVED_MARGIN:
			path.pop_back()
			
			if path.empty():
				on_arrived_at_destination()
				return
		else:
			at_destination = false
			
	var destination = path.back()
	if destination != null:
		var direction = (destination - position).normalized()
		move_and_slide(direction * 100.0)

func on_arrived_at_destination() -> void:
	emit_signal("arrived_at_destination", self)
	queue_free()
