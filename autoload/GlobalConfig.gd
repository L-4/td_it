extends Node

const TILE_SIZE := 32.0

const MAP_WIDTH := 16
const MAP_HEIGHT := 8

# @todo: Static
func get_tile_coordinates(world_coordinates: Vector2) -> Vector2:
	return Vector2(
		floor(world_coordinates.x / TILE_SIZE),
		floor(world_coordinates.y / TILE_SIZE)
	)

func get_world_coordinates(tile_coordinates: Vector2) -> Vector2:
	return tile_coordinates * TILE_SIZE + Vector2(TILE_SIZE / 2.0, TILE_SIZE / 2.0)

# Why wouldn't this be a global already
var camera: Camera2D

func screen_to_world(screen: Vector2) -> Vector2:
	return (screen - (camera.get_viewport_rect().size / 2)) * camera.zoom.x + camera.get_camera_position()

enum CollisionLayer {
	WORLD = 1,
	PLAYER = 2,
	TOWER = 4,
	ENEMY = 8,
}
