extends Node

var existing_map_reference: Node2D

var navigation_tilemap: TileMap
var build_tilemap: TileMap

func load_map(path: String) -> void:
	var map_scene = load(path)
	assert(map_scene)

	if existing_map_reference: existing_map_reference.queue_free()

	var map_instance = map_scene.instance() as Node2D
	get_node("/root/Main/Map").add_child(map_instance)

	navigation_tilemap = map_instance.get_node("Navigation")
	build_tilemap = map_instance.get_node("Build")
	build_tilemap = map_instance.get_node("Build")

	PathManager.generate_path_using_tilemap(navigation_tilemap)

func is_location_buildable(tile_coordinates: Vector2) -> bool:
	return build_tilemap.get_cell(tile_coordinates.x, tile_coordinates.y) != TileMap.INVALID_CELL

func can_build_at(tile_coordinates: Vector2) -> bool:
	return is_location_buildable(tile_coordinates) and PathManager.can_build_at(tile_coordinates)

func is_location_on_navmesh(tile_coordinates: Vector2) -> bool:
	return navigation_tilemap.get_cell(tile_coordinates.x, tile_coordinates.y) != TileMap.INVALID_CELL
