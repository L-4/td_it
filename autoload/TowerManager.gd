extends Node

signal held_tower_changed(new_tower)
signal built_tower(tower, position)
signal tower_selected(tower)
signal tower_deselected()

const TILE_SIZE := GlobalConfig.TILE_SIZE

const tower_scene = preload("res://towers/Tower.tscn")

var towers: Dictionary
var held_tower: String
var range_preview_node: RangePreview2D

func _ready() -> void:
	assert(PhaseManager.connect("build_phase_ended", self, "stop_holding_tower") == OK)

func reset() -> void:
	towers = {}
	held_tower = ""
	range_preview_node = RangePreview2D.new()

	get_node("/root/Main/Effects").add_child(range_preview_node)
	range_preview_node.visible = false

func is_any_tower_held() -> bool:
	return held_tower != ""

func get_held_tower_info() -> Object:
	if not is_any_tower_held(): return null
	return tower_info_list[held_tower]

func get_tower_info_for(tower_name: String) -> Object:
	assert(tower_info_list.has(tower_name))
	return tower_info_list[tower_name]

func try_build_at_coordinates(coordinates: Vector2) -> void:
	if not PhaseManager.is_build_phase(): return
	if not is_any_tower_held(): return
	if not MapManager.can_build_at(coordinates): return
	var node_id_at_coordinates = PathManager.get_node_id_for_point(
		coordinates.x, coordinates.y)
	var tower_key = str(coordinates.x, "-", coordinates.y)

	if towers.has(tower_key):
		return

	var tower_info = get_held_tower_info()
	if not MoneyManager.try_buy(tower_info.price): return

	var tower_instance = load(tower_info.scene).instance()
	tower_instance.name = str("tower_%d_%d" % [coordinates.x, coordinates.y])
	get_node("/root/Main/Towers").add_child(tower_instance)
	tower_instance.position = GlobalConfig.get_world_coordinates(coordinates)

	towers[tower_key] = tower_instance

	if MapManager.is_location_on_navmesh(coordinates):
		PathManager.disable_node_at(node_id_at_coordinates)
	else:
		assert(node_id_at_coordinates == -1)
	emit_signal("built_tower", tower_instance, coordinates)

	# Update preview
	resolve_preview(GlobalConfig.get_world_coordinates(coordinates))

func stop_holding_tower() -> void:
	held_tower = ""
	hide_preview()

	emit_signal("held_tower_changed", "")

func hold_tower(tower_name: String) -> void:
	assert(tower_info_list.has(tower_name))
	held_tower = tower_name

	emit_signal("held_tower_changed", tower_name)

func hide_preview() -> void:
	range_preview_node.visible = false

func resolve_preview(preview_position: Vector2) -> void:
	if not is_any_tower_held():
		hide_preview()
		return
	if not PhaseManager.is_build_phase(): return
	var tile_position = GlobalConfig.get_tile_coordinates(preview_position)
	if not MapManager.is_location_buildable(tile_position):
		hide_preview()
		return

	range_preview_node.visible = true
	range_preview_node.position = GlobalConfig.get_world_coordinates(tile_position)
	range_preview_node.is_ok = MapManager.can_build_at(tile_position)

func get_tower_at_coordinates(tile_coordinates: Vector2) -> Tower2D:
	return get_node_or_null("/root/Main/Towers/tower_%d_%d" %
		[tile_coordinates.x, tile_coordinates.y]) as Tower2D

var selected_tower: Tower2D

func resolve_select(tile_coordinates: Vector2) -> void:
	var tower = get_tower_at_coordinates(tile_coordinates)

	#   sel      	last    do
	#   n			n		return
	#   s			n		select
	#   n			s 		deselect
	#   s			s		deselect/select
	# Deselect if we were selecting, and not selecting same thing again (including null)
	if selected_tower != null and tower != selected_tower:
		try_deselect()

	# Don't reselect
	if tower != null and tower != selected_tower:
		selected_tower = tower
		emit_signal("tower_selected", tower)

func try_deselect() -> void:
	if selected_tower != null:
		selected_tower = null
		emit_signal("tower_deselected")
		
func sell_tower(tower: Tower2D) -> void:
	var tile_position = GlobalConfig.get_tile_coordinates(tower.position)
	
	var tower_info = get_tower_info_for(selected_tower.tower_name)
	MoneyManager.change_money(tower_info.price / 2)
	selected_tower.queue_free()
	TowerManager.try_deselect()
	
	var path_node_id: int = PathManager.get_node_id_for_point(tile_position.x, tile_position.y)
	if path_node_id != -1: PathManager.enable_node_at(path_node_id)

const tower_info_list = {
	"basic": {
		"scene": "res://towers/Tower.tscn",
		"name": "Basic tower",
		"price": 10
	},
	"shotgun": {
		"scene": "res://towers/ShotgunTower.tscn",
		"name": "Shotgun tower",
		"price": 20
	}
}
