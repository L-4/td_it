extends Node

signal wave_phase_complete()
signal wave_started(wave)

signal health_changed(delta, new_health)

var current_wave: int
var health: int
var enemies_left: int
var enemy_spawn_interval: float
var enemy_spawn_cooldown: float
var enemy_parent: Node2D
var waiting_for_last_enemies: bool

func change_health(delta: int) -> void:
	health += delta

	if health <= 0:
		pass # @todo deal with this

	emit_signal("health_changed", delta, health)

func _ready() -> void:
	PhaseManager.connect("wave_phase_started", self, "on_phase_started")
	set_process(false)

func reset() -> void:
	health = 100
	current_wave = 1
	enemies_left = 0
	enemy_spawn_interval = 1.0
	enemy_spawn_cooldown = 0.0
	waiting_for_last_enemies = false

	set_process(false)

func leave_state() -> void:
	set_process(false)
	emit_signal("wave_phase_complete")
	current_wave += 1
	set_process_input(false)

var enemy_scene = preload("res://enemies/Enemy.tscn")

func on_phase_started() -> void:
	waiting_for_last_enemies = false
	set_process(true)
	set_process_input(true)
	enemy_parent = get_node("/root/Main/Enemies")
	enemies_left = current_wave * 5
	enemy_spawn_interval = 0.5 + 1 / current_wave
	emit_signal("wave_started", current_wave)

func spawn_enemy() -> void:
	var enemy_instance = enemy_scene.instance()
	enemy_instance.position = Vector2(-1020.0, -686.0)

	enemy_instance.connect("arrived_at_destination", self, "on_enemy_reached_end")

	enemy_parent.add_child(enemy_instance)

func on_enemy_reached_end(enemy: Enemy2D) -> void:
	change_health(-1)

func _process(delta: float) -> void:
	if not waiting_for_last_enemies:
		enemy_spawn_cooldown -= delta

		if enemy_spawn_cooldown <= 0.0:
			enemy_spawn_cooldown = enemy_spawn_interval

			spawn_enemy()
			enemies_left -= 1

			if enemies_left <= 0:
				waiting_for_last_enemies = true
	elif enemy_parent.get_child_count() == 0:
		leave_state()
