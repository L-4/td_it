extends Node

signal money_changed(delta, new_total)

var money: int

func reset() -> void:
	money = 1000

func change_money(delta: int) -> void:
	assert(money + delta >= 0)

	money += delta
	emit_signal("money_changed", delta, money)

func can_afford(value: int) -> bool:
	return money >= value

func try_buy(value: int) -> bool:
	if can_afford(value):
		change_money(-value)
		return true
	return false
