extends Node

# @todo: Should this be here, and not in the individual files?
signal wave_phase_started()
signal wave_phase_ended()
signal build_phase_started()
signal build_phase_ended()

signal phase_changed(new_phase)

enum Phase { Wave, Build }

var current_phase: int

func _ready() -> void:
	assert(WavePhase.connect("wave_phase_complete", self, "on_wave_phase_complete") == OK)
	assert(BuildPhase.connect("build_phase_complete", self, "on_build_phase_complete") == OK)

func reset() -> void:
	set_phase(Phase.Build)

func set_phase(new_phase: int) -> void:
	assert(new_phase != current_phase)

	match current_phase:
		Phase.Build:
			emit_signal("build_phase_ended")
		Phase.Wave:
			emit_signal("wave_phase_ended")
		_:
			printerr("Tried to switch from unknown phase: ", current_phase)

	match new_phase:
		Phase.Build:
			emit_signal("build_phase_started")
		Phase.Wave:
			emit_signal("wave_phase_started")
		_:
			printerr("Tried to set phase to unknown: ", new_phase)

	emit_signal("phase_changed", new_phase)
	current_phase = new_phase

func on_wave_phase_complete() -> void:
	set_phase(Phase.Build)

func on_build_phase_complete() -> void:
	set_phase(Phase.Wave)

func get_phase_as_string(phase: int) -> String:
	match phase:
		Phase.Build: return "Build"
		Phase.Wave: return "Wave"
		_:
			printerr("Unknown phase: ", phase)
			return ""

func is_build_phase() -> bool: return current_phase == Phase.Build
func is_wave_phase() -> bool: return current_phase == Phase.Wave
