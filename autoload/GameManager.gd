extends Node

# Should reset everything
func reset() -> void:
	var autoloads = [
		BuildPhase,
		# GlobalConfig, # Global config is static data and helper functions
		PathManager,
		PhaseManager,
		TowerManager,
		WavePhase,
		MoneyManager,
	]

	for global in autoloads:
		global.reset()

func return_to_main_menu() -> void:
	get_node("/root/Main/UI/MainMenu").visible = true
	get_tree().paused = true
