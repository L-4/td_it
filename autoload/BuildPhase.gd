extends Node

signal build_phase_complete()

func _ready() -> void:
	assert(PhaseManager.connect("build_phase_started", self, "on_phase_started") == OK)

func reset() -> void:
	pass

func on_phase_started() -> void:
	set_process_unhandled_input(true)
	pass

func request_end_build_phase() -> void:
	set_process_unhandled_input(false)
	emit_signal("build_phase_complete")

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("primary_fire"):
		var tile_coordinates := GlobalConfig.get_tile_coordinates(
			GlobalConfig.screen_to_world(event.position))

		if TowerManager.is_any_tower_held():
			TowerManager.try_build_at_coordinates(tile_coordinates)
		else:
			TowerManager.resolve_select(tile_coordinates)
	elif event.is_action_pressed("cancel"):
		TowerManager.stop_holding_tower()
		TowerManager.try_deselect()
	elif event is InputEventMouseMotion:
		TowerManager.resolve_preview(GlobalConfig.screen_to_world(event.position))
