extends Node

signal navmesh_updated()

const MAP_HEIGHT := GlobalConfig.MAP_HEIGHT
const MAP_WIDTH := GlobalConfig.MAP_WIDTH

func get_node_id_for_point(x: int, y: int) -> int:
	if points.has(x) and points[x].has(y): return points[x][y]
	return -1

var astar: AStar2D

# In reverse order
var path: Array

var points := {}

const SPAWN_NODE_ID := 69420
const END_NODE_ID := 69421
onready var path_visualizer := PathVisualizer2D.new()

func _ready() -> void:
	PhaseManager.connect("wave_phase_started", self, "on_wave_phase_started")

const CARDINAL_DIRECTIONS := [
	Vector2.UP,
	Vector2.RIGHT,
	Vector2.DOWN,
	Vector3.LEFT,
]

func generate_path_using_tilemap(tilemap: TileMap) -> void:
	astar.clear()
	for point in tilemap.get_used_cells():
		var point_x = int(point.x)
		var point_y = int(point.y)
		var tile_id = astar.get_available_point_id()
		if not points.has(point_x): points[point_x] = {}
		points[point_x][point_y] = tile_id

		astar.add_point(
			tile_id,
			GlobalConfig.get_world_coordinates(point))

	for point in tilemap.get_used_cells():
		var point_x = int(point.x)
		var point_y = int(point.y)
		var tile_id = points[point_x][point_y]

		for dir in CARDINAL_DIRECTIONS:
			var neighbor = get_node_id_for_point(point_x + dir[0], point_y + dir[1])
			if neighbor != -1:
				astar.connect_points(tile_id, neighbor, false)

	astar.add_point(SPAWN_NODE_ID, Vector2(-1020.0, -686.0))
	astar.connect_points(SPAWN_NODE_ID, get_node_id_for_point(-29, -23))
	astar.add_point(END_NODE_ID, Vector2(-60, -690))
	astar.connect_points(END_NODE_ID, get_node_id_for_point(-5, -22))

	navmesh_updated()
	debug_draw_paths()

func reset() -> void:
	astar = AStar2D.new()
	path = Array()
	if debug_mesh != null: debug_mesh.queue_free()
	get_node("/root/Main/Effects").add_child(path_visualizer)

var debug_mesh: MeshInstance2D

func debug_draw_paths() -> void:
	var am = ArrayMesh.new()

	var verts := PoolVector3Array()
	var colors := PoolColorArray()

	for point in astar.get_points():
		if astar.is_point_disabled(point): continue
		var point_position := astar.get_point_position(point)

		for other_point in astar.get_point_connections(point):
			if astar.is_point_disabled(other_point): continue
			var other_point_position := astar.get_point_position(other_point)

			verts.push_back(Vector3(point_position.x, point_position.y, 0))
			colors.push_back(Color.green)
			var other_point_halfway = lerp(point_position, other_point_position, 0.4)
			verts.push_back(Vector3(other_point_halfway.x, other_point_halfway.y, 0))
			colors.push_back(Color.red)

	var arrays := []
	arrays.resize(ArrayMesh.ARRAY_MAX)
	arrays[ArrayMesh.ARRAY_VERTEX] = verts
	arrays[ArrayMesh.ARRAY_COLOR] = colors

	am.add_surface_from_arrays(Mesh.PRIMITIVE_LINES, arrays)

	if debug_mesh == null:
		debug_mesh = MeshInstance2D.new()
		get_node("/root/Main/Debug").add_child(debug_mesh)

	debug_mesh.mesh = am

func can_build_at(tile_coordinates: Vector2) -> bool:
	var id := get_node_id_for_point(tile_coordinates.x, tile_coordinates.y)
	if id == -1: return true # If there's no navmesh point here, then there's no
	# reason to not allow building here.
	# A better name would be would_block_navmesh
	assert(astar.has_point(id))
	if astar.is_point_disabled(id): return false

	astar.set_point_disabled(id, true)
	var can_path = not astar.get_id_path(SPAWN_NODE_ID, END_NODE_ID).empty()
	astar.set_point_disabled(id, false)

	return can_path

func disable_node_at(id: int) -> void:
	assert(astar.has_point(id))
	astar.set_point_disabled(id, true)
	navmesh_updated()

func enable_node_at(id: int) -> void:
	assert(astar.has_point(id))
	astar.set_point_disabled(id, false)
	navmesh_updated()

func navmesh_updated() -> void:
	debug_draw_paths()
	path_visualizer.clear()
	for point in astar.get_point_path(SPAWN_NODE_ID, END_NODE_ID):
		path_visualizer.push_position(point)
	path_visualizer.rebuild_path()
	emit_signal("navmesh_updated")
	

func on_wave_phase_started() -> void:
	path = Array(astar.get_point_path(SPAWN_NODE_ID, END_NODE_ID))

	path.invert()

func get_path_copy() -> Array:
	return path.duplicate()
